
package menu;

/**
 *
 * @author alesandra
 */
public class MaRecibo {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        clRecibo examen02 = new clRecibo();
        examen02.setNumrecibo(102);
        examen02.setNombre("jose lopez");
        examen02.setDomicilio("av del sol 1200");
        examen02.setTipo(1);
        examen02.setCosto(2.00f);
        examen02.setKillCon(450.00f);
        examen02.setFecha("21 marzo 2019");
         
        System.out.println("Num. Recibo: "+ examen02.getNumrecibo());
        System.out.println("Nombre: "+ examen02.getNombre());
        System.out.println("Domicilio: "+ examen02.getDomicilio());
        System.out.println("Tipo de servicio: "+ examen02.getTipo()+(": domicilio"));
        System.out.println("Costo por killowat: "+ examen02.getCosto());
        System.out.println("Killowats consumidos: " + examen02.getKillCon());
        System.out.println("Fecha: "+ examen02.getFecha());
        System.out.println("Calculo de pago subtotal:"+ examen02.calcularSubtotal());
        System.out.println("calculo de pago con impuesto:"+ examen02.calcularImpuesto());
        System.out.println("calculo de pago total:"+ examen02.calcularTotal());
        
        
        System.out.println("------------------------------------------------------------------------");
        
        //ejemplo 2
        examen02.setNumrecibo(103);
        examen02.setNombre("Abarrotes feliz");
        examen02.setDomicilio("av del sol");
        examen02.setTipo(2);
        examen02.setCosto(3.00f);
        examen02.setKillCon(1200);
        examen02.setFecha("21 marzo 2019");
        
        System.out.println("Num. Recibo: "+ examen02.getNumrecibo());
        System.out.println("Nombre: "+ examen02.getNombre());
        System.out.println("Domicilio: "+ examen02.getDomicilio());
        System.out.println("Tipo de servicio: "+ examen02.getTipo()+(": domicilio"));
        System.out.println("Costo por killowat: "+ examen02.getCosto());
        System.out.println("Killowats consumidos: " + examen02.getKillCon());
        System.out.println("Fecha: "+ examen02.getFecha());
        System.out.println("Calculo de pago subtotal:"+ examen02.calcularSubtotal());
        System.out.println("calculo de pago con impuesto:"+ examen02.calcularImpuesto());
        System.out.println("calculo de pago total:"+ examen02.calcularTotal());

    }
    
}
