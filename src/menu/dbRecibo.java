package menu;

/**
 *
 * @author alesandra
 */
import java.sql.*;
public class dbRecibo {
    
    
    private String MYSQLDRIVER="com.mysql.cj.jdbc.Driver";
    private String MYSQLDB="jdbc:mysql://3.132.136.208:3306/asandoval?user=asandoval&password=sandoval";
    private Connection conexion;
    private String strConsulta;
    private ResultSet registros;
    
    //constructor
    public dbRecibo(){
        
        try {
            Class.forName(MYSQLDRIVER);
            
        } catch( ClassNotFoundException e){
        
            System.out.println("Surgio un ERROR"+ e.getMessage());
            System.exit(-1);
            
        }
        
    }
       public void conectar(){
            
        try {
        conexion = DriverManager.getConnection(MYSQLDB);
        } catch(SQLException e){
            
            System.out.println("No se logró conectar "+e.getMessage());
        }
        
        }
       
       public void desconectar(){
       
       try {
           
       conexion.close();
       
       } catch(SQLException e){
       
       System.out.println("Surgió un error al desconectar "+ e.getMessage());
       }
    
}
       
       public void insertar(clRecibo rec){
           conectar();
           try{
               
       strConsulta= "INSERT INTO Recibo(numrecibo, fecha, nombre, domicilio, tipo, costo, consumo, status) "
               + "VALUES(?,CURDATE(),?,?,?,?,?,?)";
       
            PreparedStatement pst = conexion.prepareStatement(strConsulta);       
            pst.setInt(1, rec.getNumrecibo());
            pst.setString(2, rec.getNombre());
            pst.setString(3, rec.getDomicilio());
            pst.setInt(4, rec.getTipo());
            pst.setFloat(5, rec.getCosto());
            pst.setFloat(6, rec.getKillCon());
            pst.setInt(7, rec.getStatus());
            
            pst.executeUpdate();
           } catch (SQLException e) {
            System.out.println("Error al insertar "+ e.getMessage());
        }
           desconectar();
}
         public void actualizar(clRecibo rec){
             clRecibo examen02 = new clRecibo();
          
               
       strConsulta= "UPDATE Recibo SET nombre = ?,domicilio = ?,fecha = ?,tipo = " +
               "? ,costo = ?,consumo = ? WHERE numrecibo = ? and status =0;";
        this.conectar();
           try{
            PreparedStatement pst = conexion.prepareStatement(strConsulta);
            pst.setString(1, rec.getNombre());
            pst.setString(2, rec.getDomicilio());
            pst.setString(3, rec.getFecha());
            pst.setInt(4, rec.getTipo());
            pst.setFloat(5, rec.getCosto());
            pst.setFloat(6, rec.getKillCon());
            pst.setInt(7, rec.getNumrecibo());
            
            pst.executeUpdate();
            this.desconectar();
           } 
           catch (SQLException e) {
            System.out.println("surgio un error al actualizar "+ e.getMessage());
           }
         }
         
         public void habilitar(clRecibo rec){
             
             String consulta = "";
             strConsulta = "UPDATE Recibo SET status = 0 WHERE numrecibo = ?";
             this.conectar();
             try {
                 System.err.println("se conecto");
                 PreparedStatement pst = conexion.prepareStatement(strConsulta);
                 // asignar los valores a la consulta
                 pst.setInt(1, rec.getNumrecibo());
                 
                 pst.executeUpdate();
                 this.desconectar();
             } catch (SQLException e) {
                 System.err.println("surgio un error al habilitar" + e.getMessage());
             }
         }
         
             public void deshabilitar(clRecibo rec){
             strConsulta = "UPDATE Recibo SET status = 1 WHERE numrecibo = ?";
             this.conectar();
             try {
                 System.err.println("se conecto");
                 PreparedStatement pst = conexion.prepareStatement(strConsulta);
                 // asignar los valores a la consulta
                 pst.setInt(1, rec.getNumrecibo());
                 
                 pst.executeUpdate();
                 this.desconectar();
             } catch (SQLException e) {
                 System.err.println("surgio un error al habilitar" + e.getMessage());
             }
         
        }
             
             public boolean isExiste(int numrecibo, int status) {
                 boolean exito = false;
                 this.conectar();
                 strConsulta = "SELECT * FROM Recibo WHERE numrecibo = ? and status = ?;";
                 try {
                     PreparedStatement pst = conexion.prepareStatement(strConsulta);
                     pst.setInt(1, numrecibo);
                     pst.setInt(2, status);
                     this.registros = pst.executeQuery();
                     if(this.registros.next()) exito = true;
                 } catch(SQLException e){
                 System.err.println("surgio un error al verificar si existe: " + e.getMessage());
                 
             }
                 this.desconectar();
                 return exito;
}
             public clRecibo buscar(int numrecibo){
                 clRecibo examen02 = new clRecibo();
                 conectar();
                 try {
                     strConsulta = "SELECT * FROM Recibo WHERE numrecibo = ? and status = 0;";
                     PreparedStatement pst = conexion.prepareStatement(strConsulta);
                     
                     pst.setInt(1, numrecibo);
                     this.registros = pst.executeQuery();
                     if(this.registros.next()){
                         
                         examen02.setId(registros.getInt("id"));
                         examen02.setNumrecibo(registros.getInt("numrecibo"));
                         examen02.setNombre(registros.getString("nombre"));
                         examen02.setDomicilio(registros.getString("domicilio"));
                         examen02.setTipo(registros.getInt("tipo"));
                         examen02.setCosto(registros.getFloat("costo"));
                         examen02.setKillCon(registros.getFloat("consumo"));
                         examen02.setFecha(registros.getString("fecha"));
                         
                         
                     } else examen02.setId(0);
                 }
                 catch(SQLException e){
                     System.err.println("surgio un error al habilitar: " + e.getMessage());
                     
                 }
                 this.desconectar();
                 return examen02;
             }
}

