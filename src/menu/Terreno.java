/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package menu;

/**
 *
 * @author alesandra
 */
public class Terreno {
    
    public int numlote;
    public float ancho;
    public float largo;
    
    
    public Terreno(){
    
    this.ancho=0.0f;
    this.largo=0.0f;
    this.numlote=0;
        
    }
    
    public Terreno(int numlote,float ancho,float largo){
        
        this.ancho=ancho;
        this.largo=largo;
        this.numlote=numlote;
        
        
    }

    public int getNumlote() {
        return numlote;
    }

    public void setNumlote(int numlote) {
        this.numlote = numlote;
    }

    public float getAncho() {
        return ancho;
    }

    public void setAncho(float ancho) {
        this.ancho = ancho;
    }

    public float getLargo() {
        return largo;
    }

    public void setLargo(float largo) {
        this.largo = largo;
    }
    
    public  float calcularArea(float ancho, float largo){
        float area=0.0f;
        area=this.ancho * this.largo;
        return area;
    }
     public float calcularPerimetro(float ancho, float largo){
        float perimetro=0.0f;
        perimetro=this.ancho*2 + this.largo*2;
        return perimetro;
    }
}