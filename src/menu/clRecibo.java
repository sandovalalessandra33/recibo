
package menu;

/**
 *
 * @author alesandra
 */
public class clRecibo {
    
    private int id;
    private int numrecibo;
    private String fecha;
    private String nombre;
    private String domicilio;
    private int tipo;
    private float costo;
    private float killCon;
    private int status;
  
    
    public clRecibo(){
    this.id=0;
    this.numrecibo=0;
    this.nombre="";
    this.domicilio="";
    this.tipo=0;
    this.costo=0.0f;
    this.fecha="";
    this.killCon=0.0f;
    this.status=0;
    
    
    }
    //Constructor por argumentos
    public clRecibo(int id, int numrecibo, String nombre, String domicilio, int tipo, float costo, String fecha, int killCon, int status){
    this.id=id;
    this.numrecibo=numrecibo;
    this.nombre=nombre;
    this.domicilio=domicilio;
    this.tipo=tipo;
    this.costo=costo;
    this.fecha=fecha;
    this.killCon=killCon;
    this.status=status;
   
    }
    
    //Copia "OTRO"
    public clRecibo(clRecibo otro){
    this.id=otro.id;
    this.numrecibo=otro.numrecibo;
    this.nombre=otro.nombre;
    this.domicilio=otro.domicilio;
    this.tipo=otro.tipo;
    this.costo=otro.costo;
    this.fecha=otro.fecha;
    this.killCon=otro.killCon;
    this.status=otro.status;
    }
    

    //Metodos Set y Get
     public int getId() {    
        return id;
    }

    public int getStatus() {
        return status;
    }
   
    public int getNumrecibo() {
        return numrecibo;
    }

    public void setNumrecibo(int numrecibo) {
        this.numrecibo = numrecibo;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getDomicilio() {
        return domicilio;
    }

       public void setId(int id) {
        this.id = id;
    }
    
     public void setStatus(int status) {
        this.status = status;
    }

    
    public void setDomicilio(String domicilio) {
        this.domicilio = domicilio;
    }

    public int getTipo() {
        return tipo;
    }

    public void setTipo(int tipo) {
        this.tipo = tipo;
    }

    public float getCosto() {
        return costo;
    }

    public void setCosto(float costo) {
        this.costo = costo;
    }

    public float getKillCon() {
        return killCon;
    }

    public void setKillCon(float killCon) {
        this.killCon = killCon;
    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    
    

    
    //Metodos de comportamiento

     public float calcularSubtotal(){
    float subtotal=0.0f;
    if (this.tipo == 1) {
        subtotal=(this.killCon*this.costo);
    }
    if (this.tipo == 2) {
        subtotal=(this.killCon*this.costo);
    }
    if (this.tipo == 3) {
         subtotal=(this.killCon*this.costo);
    }
    return subtotal;
    }
    
    public float calcularImpuesto(){
    float impuesto=0.0f;
    impuesto=this.calcularSubtotal()*0.16f;
    return impuesto;
    }
    
    public float calcularTotal(){
    float total=0.0f;
    total=this.calcularSubtotal()+this.calcularImpuesto();
    return total;
    }
    
}

